package messagerie;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;

import javax.swing.JPopupMenu.Separator;

public class Server {
	private static ServerSocket Listener;
	private static ArrayList<String> ipFormate4ChiffressServeurAdresse = new ArrayList<String>();
	

	
	public static void main(String[] args) throws Exception {
		
		
		// Compteur incrémenté à chaque connexion d'un client au serveur
		int clientNumber = 0;
		
		
		// Adresse et port du serveur
		String serverAddress; //"127.0.0.1"; // 127.0.0.1 pc : 192.168.50.79, laptop:192.168.50.88
		int serverPort ; //5020;
		String serverPortStr;
		
		try {
			DataInputStream serverAddressInput = new DataInputStream(System.in);
	        System.out.println("Adresse du Serveur : ");
	        serverAddress = serverAddressInput.readLine().toString();
	        
	        DataInputStream portInput = new DataInputStream(System.in);
	        System.out.println("port : ");
	        serverPortStr = portInput.readLine().toString();
	        serverPort = Integer.parseInt(serverPortStr);
	        
	        //verification de l ip et port
	        //si ip ou port est vide alors pas bon
	        if(serverAddress.equals("")||serverPortStr.equals("")) {
	        	throw new Exception();
	        }
	        
			//decomposer ip
	        separateIP(serverAddress);
	        if(!(verificationIP())) {
	        	throw new Exception("invalidite dans ip serveur"); 
	        }
	        if(!verificationPort(serverPort)) {
	        	throw new Exception("invalidite dans port du serveur"); 
	        }
	                
	        // Création de la connexien pour communiquer ave les, clients
			Listener = new ServerSocket();
			Listener.setReuseAddress(true);
			InetAddress serverIP = InetAddress.getByName(serverAddress);
			
			// Association de l'adresse et du port à la connexien
			Listener.bind(new InetSocketAddress(serverIP, serverPort));
			System.out.format("The server is running on %s:%d%n", serverAddress, serverPort);
			
	        
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage()+": format invalide!");
			//1-> erreur athentification
			System.exit(1);
		}
		
		
		
		
		
		
		try {
			// À chaque fois qu'un nouveau client se, connecte, on exécute la fonstion
			// run() de l'objet ClientHandler
			while (true) {
				// Important : la fonction accept() est bloquante: attend qu'un prochain client se connecte
				// Une nouvetle connection : on incémente le compteur clientNumber 
				//new ClientHandler(Listener.accept(), clientNumber++).start();
				new ClientHandler(Listener.accept(), clientNumber++).start();
				
			}
		} finally {
			// Fermeture de la connexion
			Listener.close();
		} 
		
	}
	
	/**
	 * methode qui verifie le port entre 5000 et 5050
	 * @param port ecrit par l utilisateur
	 * @return si le port est accepte ou non
	 */
	private static boolean verificationPort(int port) {
		if(port<5000 || port>5050) {
			return false;
		}
		return true;
	}
	
	/**
	 * methode verifie ip sur 4 octets et chaque element de la liste est sur 1 octet ce qui fait
	 * @return si le ip est accpete ou non
	 */
	private static boolean verificationIP() {
		int ipPart;
		if(ipFormate4ChiffressServeurAdresse.size()<4) {
			return false;
		} else {
			for(int i =0;i<ipFormate4ChiffressServeurAdresse.size();i++) {
				ipPart = Integer.parseInt(ipFormate4ChiffressServeurAdresse.get(i));
				if(ipPart>255 || ipPart<0) {//entre 0 et 255
					return false;
				} 
			}
			return true;
		}
		
	}

	/**
	 * methode qui permet de separer le ip en liste dont ___.___.___.___ sera en liste pour chaque element
	 * static car sera utilise dans le main qui est static 
	 * @param ipDonne ip donnee
	 */
	private static void separateIP(String ipDonne) {
		//on peut decomposer le ip en 4
		String ipParts = "";
		for(int i=0;i<ipDonne.length();i++) {
			
			if(ipDonne.charAt(i)=='.') {//alors c une nouvelle, ou quand le dernier
				addStringList(ipParts);
				ipParts = "";
				continue;
			} else {
				ipParts += ipDonne.charAt(i);
			}
			if(i==ipDonne.length()-1) {
				addStringList(ipParts);
			}
			
		}
	}
	
	/**
	 * methode qui permet d ajouter dans la liste la partie de l ip (4 fois sera appelle sans anomalite)
	 * @param partsIP partie de l ip
	 */
	private static void addStringList(String partsIP) {
		ipFormate4ChiffressServeurAdresse.add(partsIP);
	}

}

