package messagerie;
import java.io.DataInputStream;
import java.util.Date;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;


public class Client {

	private static Socket socket;
	public static void main(String[] args) throws Exception {
		Date dateMtn=new Date();

		String serveurAdresseStr ;//127.0.0.1(localhost)  pc : 192.168.50.79, laptop:192.168.50.88
		int port ;
		String dateHour = dateMtn.toString();
		
		
		//demander util et mdp
		String utilStr,mdpStr,portServeurStr;
		try {
			DataInputStream serveurAdresse = new DataInputStream(System.in);
			System.out.println("adresse du serveur : ");
			serveurAdresseStr = serveurAdresse.readLine().toString();
			
			DataInputStream portServeur = new DataInputStream(System.in);
			System.out.println("port : ");
			portServeurStr = portServeur.readLine().toString();
			port = Integer.parseInt(portServeurStr);
			
			//verif ip et port
			if(serveurAdresseStr.equals("")||portServeurStr.equals("")) {
	        	throw new Exception();
	        }
	        
	        if(!(separateIPAndVerify(serveurAdresseStr))) {
	        	throw new Exception("invalidite dans ip serveur"); 
	        }
	        if(!verificationPort(port)) {
	        	throw new Exception("invalidite dans port du serveur"); 
	        }
	        
	        socket = new Socket(serveurAdresseStr, port);
			System.out.format("Serveur lancé sur [%s:%d]", serveurAdresseStr, port);
			System.out.println();//saut de ligne
			
			DataInputStream util = new DataInputStream(System.in);
	        System.out.println("util : ");
	        utilStr = util.readLine().toString();
	        
	        DataInputStream mdp = new DataInputStream(System.in);
	        System.out.println("mdp : ");
	        mdpStr = mdp.readLine().toString();
	        
	        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
	        
	        // Céatien d'un canal entrant pour recevoir les messages envoyés, par le serveur
	     	DataInputStream in = new DataInputStream(socket.getInputStream());
			
	        // Envoi du nom d'utilisateur et du mot de passe au serveur
	        out.writeUTF(utilStr);
	        out.writeUTF(mdpStr);
	        if(utilStr.equals("") || mdpStr.equals("")) {
	        	
	        	throw new Exception("erreur");
	        	
	        }
     
			String mess = in.readUTF();
			//System.out.println(mess);

			if((mess.equals("R"))) {//alors on system exit avec erreur
				System.out.println("Erreur dans la saisie du mot de passe");
				System.exit(1);//1 erreur d authentif
			} else {
				String helloMessageFromServer = mess;
				System.out.println(helloMessageFromServer);
				int nbChat = in.readInt();
				for(int i=0;i<nbChat;i++) {
					System.out.println(in.readUTF());
				}
			
				out.writeUTF(dateHour);
			
				//[Nom d’utilisateur - Adresse IP : Port client - Date et Heure (min, sec)]: message
				
				DataInputStream chat = new DataInputStream(System.in);
				while(true) {
					System.out.println("message : ");
					String messageClient = chat.readLine().toString();
					out.writeUTF(messageClient);
					
				}
				
				
				
			}
			out.writeUTF("close");
			System.exit(0);
		} catch (Exception e) {
			System.out.println(e.getMessage()+": format invalide! ");
			//1-> erreur athentification
			System.exit(1);
			
		}
		
		// fermeture de La connexion avec le serveur
		socket.close();
		
		
	}

	private static boolean verificationPort(int port) {
		if(port<5000 || port>5050) {
			return false;
		}
		return true;
	}

	private static boolean separateIPAndVerify(String ipDonne) {
		ArrayList<String> ipFormat = new ArrayList<String>();//pas besoin de .clear()
		//on peut decomposer le ip en 4
		String ipPartsStr = "";
		int ipPart;
		for(int i=0;i<ipDonne.length();i++) {

			if(ipDonne.charAt(i)=='.') {//alors c une nouvelle, ou quand le dernier
				ipFormat.add(ipPartsStr);
				ipPartsStr = "";
				continue;
			} else {
				ipPartsStr += ipDonne.charAt(i);
			}
			if(i==ipDonne.length()-1) {
				ipFormat.add(ipPartsStr);
			}

		}
		for(int i =0;i<ipFormat.size();i++) {
			ipPart = Integer.parseInt(ipFormat.get(i));
			if(ipPart>255 || ipPart<0) {//entre 0 et 255
				return false;
			} 
		}
		return true;
		
	}

}
