package messagerie;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JPanel;

import java.io.*;


public class ClientHandler extends Thread{
	
	private Socket socket;
	private int clientNumber;
	private static BufferedWriter writerAuthtification; //ecrire par dessus //static appartient a la classe 
														//et non l instance de clientHnadler
	private static BufferedReader readerAuthtification;
	private ArrayList<String> ipFormate4Chiffres;
	//private int nbClientEnregistre;
	
	private static ArrayList<String> usernames =  new ArrayList<String>();
	private static ArrayList<String> passwords = new ArrayList<String>();
	
	private static BufferedWriter writerChat;
	private static BufferedReader readerChat;
	private static ArrayList<String> chat;//n appartient pas a l instance clientHandler mais bien la classe
	//ne pas oublier a clear
	private static ArrayList<String> dernier15Chat;
	
	
	public ClientHandler(Socket socket, int clientNumber) throws IOException {
		
		this.socket = socket;
		this.clientNumber = clientNumber;
		System.out.println("New connection with client#" + clientNumber + " at" + socket);
		
		
		try {
			chat = new ArrayList<String>();
			dernier15Chat = new ArrayList<String>();
			
			
            // true ->append // authentification
            FileWriter fileWriter = new FileWriter("output.txt", true);
            writerAuthtification = new BufferedWriter(fileWriter);
            FileReader fileReader = new FileReader("output.txt");
            readerAuthtification = new BufferedReader(fileReader);
            
            //chat
            FileWriter chatWriter = new FileWriter("chat.txt", true);
            this.writerChat = new BufferedWriter(chatWriter);
            FileReader chatReader = new FileReader("chat.txt");
            this.readerChat = new BufferedReader(chatReader);
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
		
		ipFormate4Chiffres = new ArrayList<String>();
		
	}
	
	private void setChatDefault(){
		String ligneActuel;
		try {
			while ((ligneActuel=readerChat.readLine()) != null) {
				chat.add(ligneActuel);
			}
		} catch (IOException e) {
            System.out.println(e.getMessage());
		}
	}
	
	/**
	 * ajouter le chat dans le txt
	 * @param texte le texte a ajouter dans le txt
	 */
	private void addChat(String texte) {
		chat.add(texte);
	}
	
	enum user {
		  CLIENT,
		  NEW,
		  WRONG_PASSWORD,
		  ERROR
		}
	
	/**
	 * valide si le username et password correst et agit en fonction du Enum user donc si client, new ou wrong password
	 * @param username 
	 * @param password
	 * @return la validite, si wrong ou client ou nouveau
	 */
	private user validate(String username, String password) {//faudra lire dans le fichier
        //tant que le read n est pas fini on continue
		String ligneActuel="";
		String mdp ;
		try {	
			while((ligneActuel=readerAuthtification.readLine()) != null) {
				// on pourrait retenir tous les username dans une arraylist
				usernames.add(ligneActuel);
				mdp = readerAuthtification.readLine();
				passwords.add(mdp);
				
			} 	
			//donc verifie usernames
			for(int i =0 ; i<usernames.size();i++) {
				if(username.equals(usernames.get(i))&& password.equals(passwords.get(i))) {
					usernames.clear();
					passwords.clear();
					return user.CLIENT;
				} else if(username.equals(usernames.get(i))&& !(password.equals(passwords.get(i)))) {//wrong
					//on garde en memoire le username
					username = usernames.get(i);
					usernames.clear();
					passwords.clear();
					return user.WRONG_PASSWORD;
				} 
			}
			usernames.clear();
			passwords.clear();
			return user.NEW;
			
		} catch (IOException e) {
            System.out.println(e.getMessage());

		}
		return user.ERROR;
		
    }
	
	/**
	 * ecrit dans le txt des authentification nomme output.txt
	 * @param util utilsiateur
	 * @param mdp mot de passe
	 * @throws IOException exepction dans le cas erreur
	 */
	private void write(String util,String mdp ) throws IOException {//ecrire dans un fichier
		try {
			writerAuthtification.write(util);
			writerAuthtification.newLine();
			writerAuthtification.write(mdp);
			writerAuthtification.newLine();//pr la prochaine ecriture encore
			writerAuthtification.flush();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	/**
	 * ajouter dans la liste 
	 * @param str 
	 */
	private void addStringList(String str) {
		ipFormate4Chiffres.add(str);
	}
	
	/**
	 * separer ip appele addStringList
	 */
	private void separateIP() {
		//on fait la verification du format de l'ip
		String ipDonne = this.socket.getInetAddress().getHostAddress();
//		//on peut decomposer le ip en 4
		String ipParts = "";
		for(int i=0;i<ipDonne.length();i++) {
			
			if(ipDonne.charAt(i)=='.') {//alors c une nouvelle, ou quand le dernier
				addStringList(ipParts);
				ipParts = "";
				continue;
			} else {
				ipParts += ipDonne.charAt(i);
			}
			if(i==ipDonne.length()-1) {
				addStringList(ipParts);
			}
			
		}
	}
	
	public void run() { // Création de thread qui envoi un message à un client
		try {
			
			setChatDefault();
			//canal entrant du client
			DataInputStream in = new DataInputStream(socket.getInputStream());
            // Lire le nom d'utilisateur et le mot de passe envoyés par le client
            String username = in.readUTF();
            String password = in.readUTF();
            DataOutputStream out = new DataOutputStream(socket.getOutputStream()); // création de canal d’envoi 
            
            user client = validate(username, password);
            //System.out.println(client);
            //validation du mot de passe
            if(client== user.WRONG_PASSWORD) {
            	System.out.println("Erreur dans la saisie du mot de passe");
            	out.writeUTF("R");//R pour refuse pour que CLIENT interprete 
            	socket.close();
            } else if(client == user.CLIENT) {
            	out.writeUTF("Hello from server - you are already a client#" + clientNumber);
                dernier15Chat.addAll(chat.subList(Math.max(chat.size() - 15, 0), chat.size()));
                out.writeInt(dernier15Chat.size());
                for(int i =0 ; i<dernier15Chat.size();i++) {
                	Thread.sleep(200);
                	out.writeUTF(dernier15Chat.get(i));
                }
                
                
                //lire ce que le client dit : date en premier et ensuite le chat
                //[Nom d’utilisateur - Adresse IP : Port client - Date et Heure (min, sec)]: message
                String date = in.readUTF();
                String ipClient = socket.getInetAddress().toString().substring(1);//enleve le \ au debut
                String portClient = socket.getPort()+"";
                String me;
                while(!socket.isClosed()) {
                	me = in.readUTF();
                	//il va essayer de lire du input du client
                	System.out.printf("["+username+" - "+ipClient+" : "+portClient+" - "+date+"]: "+me);
                	System.out.println();
                	//on a plus qu a write le message dans le chat pour sauvegarder
                	writerChat.newLine();
                	writerChat.write(me);
                	writerChat.flush();
                	
                }
                
            } else if(client == user.NEW) {
            	//on prend le mot de passe qu il a ecrit et on lui fait un nouveau compte
            	write(username, password);
            	out.writeUTF("nouveau compte cree pour " +username+" AVEC LE MDP "+password);
            }
		
		}catch (IOException e) {
			System.out.println("Error handling client# " + clientNumber + ": " + e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				dernier15Chat.clear();
				chat.clear();
				socket.close();
			} catch (IOException e) {
				System.out.println("Couldn't close a socket, what's going on?");
			}
			System.out.println("Connection with client# " + clientNumber+ " closed");
			
		}
	}
	
	

}
